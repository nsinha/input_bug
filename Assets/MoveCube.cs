﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MoveCube : MonoBehaviour
{
    
    private InputTestActions _inputActions;

    private Vector3 _dir;
    // Start is called before the first frame update
    private void Awake()
    {
        _inputActions = new InputTestActions();
    }

    private void OnEnable()
    {
        _inputActions.Enable();

        _inputActions.Player.Movement.performed += MovementOnperformed;
    }
    
    private void MovementOnperformed(InputAction.CallbackContext context)
    {
        var value = context.ReadValue<Vector2>();
        // Debug.Log($"Moving {value}");

        _dir = new Vector3(value.x, 0, value.y);
        Debug.Log(_dir);
        

    }
    
    // Update is called once per frame
    void Update()
    {
        transform.position += (_dir * Time.deltaTime * 10.0f);
    }
    
    private void OnDisable()
    {
        _inputActions.Player.Movement.performed -= MovementOnperformed;
    }

}
